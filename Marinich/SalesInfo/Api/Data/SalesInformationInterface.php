<?php
/**
 * Created by PhpStorm.
 * User: marinich
 * Date: 22.03.19
 * Time: 19:20
 */

namespace Marinich\SalesInfo\Api\Data;

use Magento\Framework\Api\ExtensionAttributesInterface;

/**
 * Interface SalesInformationInterface
 * @package Marinich\SalesInfo\Api\Data
 */
interface SalesInformationInterface extends ExtensionAttributesInterface
{

    /**
     * Quantity field
     */
    const QTY = 'qty';

    /**
     *  Last Order Date field
     */
    const LAST_ORDER = 'last_order';

    /**
     * Get Sold Quantity
     *
     * @return float
     */
    public function getQty();

    /**
     * Set Sold Quantity
     *
     * @param integer $qty
     * @return mixed
     */
    public function setQty($qty);

    /**
     * Get Last Order Date
     *
     * @return string|null
     */
    public function getLastOrder();

    /**
     * Set Last Order Date
     *
     * @param string $lastOrderDate
     * @return mixed
     */
    public function setLastOrder($lastOrderDate);
}
