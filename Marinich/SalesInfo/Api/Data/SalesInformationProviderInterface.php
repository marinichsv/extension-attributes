<?php
/**
 * Created by PhpStorm.
 * User: marinich
 * Date: 25.03.19
 * Time: 21:50
 */

namespace Marinich\SalesInfo\Api\Data;

/**
 * Interface SalesInformationProviderInterface
 *
 * @package Marinich\SalesInfo\Api\Data
 */
interface SalesInformationProviderInterface
{

    /**
     * Get Sales Information for product
     *
     * @param $productId
     * @param null $orderStatus
     * @return mixed
     */
    public function getSalesInformation($productId, $orderStatus = null);
}
