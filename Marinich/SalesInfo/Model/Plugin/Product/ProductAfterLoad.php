<?php
/**
 * Created by PhpStorm.
 * User: marinich
 * Date: 22.03.19
 * Time: 23:03
 */

namespace Marinich\SalesInfo\Model\Plugin\Product;

use Marinich\SalesInfo\Api\Data\SalesInformationInterface;
use Marinich\SalesInfo\Api\Data\SalesInformationProviderInterface;

/**
 * Class ProductAfterLoad
 * @package Marinich\SalesInfo\Model\Plugin\Product
 */
class ProductAfterLoad
{
    /**
     * @var SalesInformationProviderInterface|null
     */
    protected $salesInformationProvider = null;

    /**
     * Repository constructor.
     * @param SalesInformationProviderInterface $provider
     */
    public function __construct(
        SalesInformationProviderInterface $provider
    ) {
        $this->salesInformationProvider = $provider;
    }

    /**
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return \Magento\Catalog\Api\Data\ProductInterface
     */
    public function afterLoad(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        $this->addSalesInfo($product);

        return $product;
    }

    /**
     * Add Sales Information for particular product
     *
     * @param \Magento\Catalog\Api\Data\ProductInterface $product
     * @return self
     */
    private function addSalesInfo(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        /** @var SalesInformationInterface $salesInfo */
        $salesInfo = $this->salesInformationProvider->getSalesInformation($product->getId());
        if ($salesInfo) {
            $extensionAttributes = $product->getExtensionAttributes();
            $extensionAttributes->setSalesInformation($salesInfo);
            $product->setExtensionAttributes($extensionAttributes);
        }

        return $this;
    }
}
