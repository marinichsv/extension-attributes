<?php
/**
 * Created by PhpStorm.
 * User: marinich
 * Date: 22.03.19
 * Time: 22:54
 */

namespace Marinich\SalesInfo\Model;

use Magento\Framework\Model\AbstractExtensibleModel;
use Marinich\SalesInfo\Api\Data\SalesInformationInterface;

/**
 * Class SalesInformation
 * @package Marinich\SalesInfo\Model
 */
class SalesInformation extends AbstractExtensibleModel implements SalesInformationInterface
{

    /**
     * Get Sold Quantity
     *
     * @return float
     */
    public function getQty()
    {
        return (float)$this->getData(static::QTY);
    }

    /**
     * Set Sold Quantity
     *
     * @param float $qty
     * @return SalesInformation|mixed
     */
    public function setQty($qty)
    {
        return $this->setData(static::QTY, $qty);
    }

    /**
     * Get Last Order Date
     *
     * @return string|null
     */
    public function getLastOrder()
    {
        return $this->getData(static::LAST_ORDER);
    }

    /**
     * Set Last Order Date
     *
     * @param string $lastOrderDate
     * @return SalesInformation|mixed
     */
    public function setLastOrder($lastOrderDate)
    {
        return $this->setData(static::LAST_ORDER, $lastOrderDate);
    }
}
