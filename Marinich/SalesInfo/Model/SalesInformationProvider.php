<?php
/**
 * Created by PhpStorm.
 * User: marinich
 * Date: 25.03.19
 * Time: 21:49
 */

namespace Marinich\SalesInfo\Model;

use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderItemInterface;

use Marinich\SalesInfo\Api\Data\SalesInformationProviderInterface;
use Marinich\SalesInfo\Api\Data\SalesInformationInterfaceFactory;

/**
 * Class SalesInformationProvider
 * @package Marinich\SalesInfo\Model
 */
class SalesInformationProvider implements SalesInformationProviderInterface
{
    /**
     * @var \Magento\Sales\Model\ResourceModel\Order
     */
    protected $orderResource;

    /**
     * @var \Magento\Sales\Model\ResourceModel\Order\Item
     */
    protected $itemResource;

    /**
     * @var SalesInformationInterfaceFactory
     */
    protected $salesInformationFactory;

    /**
     * Sales Information
     *
     * @var array
     */
    private $data = null;

    /**
     * Order status, to filter
     *
     * @var string
     */
    private $orderStatus = null;

    /**
     * DB Meta data of Order table
     *
     * @var array
     */
    private $orderMeta = null;

    /**
     * DB Meta data of OrderItem table
     *
     * @var array
     */
    private $orderItemMeta = null;

    /**
     * @var \Psr\Log\LoggerInterface|null
     */
    private $logger = null;

    /**
     * SalesInformationProvider constructor.
     * @param \Magento\Sales\Model\ResourceModel\Order $orderResource
     * @param \Magento\Sales\Model\ResourceModel\Order\Item $itemResource
     * @param \Psr\Log\LoggerInterface $logger
     * @param SalesInformationInterfaceFactory $salesInformationFactory
     * @param null|string $orderStatus
     */
    public function __construct(
        \Magento\Sales\Model\ResourceModel\Order $orderResource,
        \Magento\Sales\Model\ResourceModel\Order\Item $itemResource,
        \Psr\Log\LoggerInterface $logger,
        SalesInformationInterfaceFactory $salesInformationFactory,
        $orderStatus = null
    ) {
        $this->orderResource = $orderResource;
        $this->itemResource = $itemResource;
        $this->salesInformationFactory = $salesInformationFactory;
        $this->orderStatus = $orderStatus;
        $this->logger = $logger;
    }

    /**
     * Get Sales Information
     *
     * @param $productId
     * @param null $orderStatus
     * @return mixed
     */
    public function getSalesInformation($productId, $orderStatus = null)
    {
        $cacheKey = $orderStatus ? "{$productId}_{$orderStatus}" : $productId;
        if (!isset($this->data[$cacheKey])) {
            $info = $this->loadSalesInformation($productId, $orderStatus ?: $this->orderStatus);
            $this->data[$cacheKey] = $info;
        }

        return $this->data[$cacheKey];
    }

    /**
     * Load Sales Information
     *
     * @param $productId
     * @param null $orderStatus
     * @return \Marinich\SalesInfo\Api\Data\SalesInformationInterface|null
     */
    protected function loadSalesInformation($productId, $orderStatus = null)
    {
        $result = null;

        try {
            $connection = $this->orderResource->getConnection();
            $row = $connection->fetchAll(
                $this->buildSelect($this->getOrderMeta(), $this->getOrderItemMeta(), $productId, $orderStatus)
            );
            $row = $row[0];

            if (isset($row['order_id'])) {

                /** @var \Marinich\SalesInfo\Api\Data\SalesInformationInterface $salesInfo */
                $salesInfo = $this->salesInformationFactory->create();
                $salesInfo->setQty($row['qty']);
                $salesInfo->setLastOrder($row['order_created_at']);

                $result = $salesInfo;
            }
        } catch (\Exception $exception) {
            $this->logger->error($exception->getTraceAsString());
        }

        return $result;
    }

    /**
     * Build Select to load sales information
     *
     * @param $orderMeta
     * @param $orderItemMeta
     * @param $productId
     * @param $orderStatus
     * @return \Magento\Framework\DB\Select
     */
    protected function buildSelect($orderMeta, $orderItemMeta, $productId, $orderStatus)
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var  \Magento\Sales\Model\ResourceModel\Order\Collection $collection */
        $collection = $objectManager->create(\Magento\Sales\Model\ResourceModel\Order\Collection::class);
        $select = $collection->getSelect();
        $select->reset(\Magento\Framework\DB\Select::COLUMNS);
        $select->columns(
            array(
                "{$orderMeta['table']}.{$orderMeta['order_id']} as order_id",
                "max(`{$orderMeta['table']}`.`{$orderMeta['created_at']}`) as order_created_at"
            )
        );
        $select->join(
            $orderItemMeta['table'],
            "`{$orderItemMeta['table']}`.`{$orderItemMeta['order_id']}` "
              . "= `{$orderMeta['table']}`.`{$orderMeta['order_id']}`",
            array(
                $orderItemMeta['item_id'],
                "count(`{$orderItemMeta['table']}`.`{$orderItemMeta['qty_ordered']}`) as qty "
            )
        );
        $select->where("`{$orderItemMeta['table']}`.`{$orderItemMeta['product_id']}` = ?", $productId);

        if ($orderStatus) {
            $select->where("`{$orderMeta['table']}`.`{$orderMeta['status']}` = ?", $orderStatus);
        }

        $select->order('order_created_at desc');

        return $select;
    }


    /**
     * Get Meta Data of Order Table
     * @return array
     */
    protected function getOrderMeta()
    {
        if (!$this->orderMeta) {
            $this->orderMeta = array(
                'table' => 'main_table',
                'order_id' => OrderInterface::ENTITY_ID,
                'created_at' => OrderInterface::CREATED_AT,
                'status' => OrderInterface::STATUS
            );
        }

        return $this->orderMeta;
    }

    /**
     * Get Meta Data of Order Item Table
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getOrderItemMeta()
    {
        if (!$this->orderItemMeta) {
            $this->orderItemMeta = array(
                'table' => $this->itemResource->getMainTable(),
                'order_id' => OrderItemInterface::ORDER_ID,
                'item_id' => OrderItemInterface::ITEM_ID,
                'product_id' => OrderItemInterface::PRODUCT_ID,
                'qty_ordered' => OrderItemInterface::QTY_ORDERED
            );
        }

        return $this->orderItemMeta;
    }
}
